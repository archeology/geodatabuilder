import io
import pandas as pd

def parse(content: str) -> pd.DataFrame:
    data = pd.read_csv(io.StringIO(content), delimiter=' ')
    def construct(row: pd.Series):
            _, row = row
            x = row['XX']
            y = row['YY']
            out = []
            for i in range(1, 14+1):
                col_name = f'Mod_F{i}'
                if col_name not in row.index:
                    col_name = f'Mod_F0{i}'
                    if col_name not in row.index:
                        out.append((x, y, None, None))
                        continue
                out.append((x, y, i, row[col_name]))
            return pd.DataFrame(out, columns=['x', 'y', 'z', 'value'])
            
    return pd.concat(map(construct, data.iterrows()))