import pandas as pd
import re

def parse(content: str) -> pd.DataFrame:
    lines = content.split('\n')
    data = []
    for line in lines:
        if line:
            parts = line.split('|')
            if len(parts) < 6 or not parts[2].strip().startswith('PI1'):
                continue
            try:
                record = {
                    'x': float(re.findall(r'-?\d+\.\d+', parts[3])[0]),
                    'y': float(re.findall(r'-?\d+\.\d+', parts[4])[0]),
                    'h': float(re.findall(r'-?\d+\.\d+', parts[5])[0]),
                }
                data.append(record)
            except IndexError:
                continue
    return pd.DataFrame(data)