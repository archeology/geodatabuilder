"""
Парсер для svg файлов созданных из импорта в CoralDraw.
Для успешного парсинга необходимо разметить xml svg в соответствии со слействующими правилами:

1. Добавлен класс startpoint для полигона начала координат (место установки тахеометра)
2. Добавлен класс scalepoint1 для линии первой масштабирующей точки на маштабной линейки
3. Добавлен класс scalepoint2 для линии второй масштабирующей точки на маштабной линейки (отметка 10м)
4. Всем полигонам интереса надо добавить значение атрибута class вида aicls<номер класса>
    - aicls1 - жилище
    - aicls2 - ров
    - aicls3 - оборонительные стены
    - aicls4 - могильники
    - aicls5 - внутренние стены
    - aicls6 - колодцы
    - aicls7 - входы в поселение

"""
from collections import defaultdict
import json
from matplotlib import pyplot as plt
import numpy as np
from svgelements import SVG, Text, Polygon as SVGPolygon, Ellipse as SVGEllipse, Path as SVGPath, Line as SVGLine, Point as SVGPoint
from svgpathtools import Path as SVGPath, svg2paths
from matplotlib.patches import Ellipse, Patch
import matplotlib as mpl
from shapely import Polygon as Polygon, LinearRing, Point
from typing import Dict, List, Tuple, Union
from itertools import groupby
from pathlib import Path


SYSTEMCLASS = ['startpoint', 'scalepoint1', 'scalepoint2']
# 1.Жилище  (жилищная впадина)
# 2. Ров
# 3. Оборонительные стены
# 4. Могильники
# 5. Внутренние стены
# 6. Колодцы
# 7. Входы в поселение
AICLS = ['aicls1', 'aicls2', 'aicls3', 'aicls4', 'aicls5', 'aicls6', 'aicls7']
FILTERED_CLASSES = AICLS + SYSTEMCLASS


# class Polygon(_Polygon):

#     def __new__(cls, shell=None, holes=None, classes=None, polygon=None, **kwargs):
#         out = _Polygon.__new__(cls, shell=shell, holes=holes)
#         setattr(out.__class__, 'classes', list())
#         out.classes = classes
#         return out

#         # setattr(out.__class__, 'get_cls', lambda key, default=None: classes.get(key, default))

#     def __init__(self, *args, polygon = None, classes=None, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.classes = classes
#         if self.classes is None:
#             self.classes = []
#         self._polygon = polygon
        
#     def __getitem__(self, key):
#         return self._polygon.__getitem__(key)
    
#     def __setitem__(self, key, value):
#         self._polygon.__setitem__(key, value)
        
#     def __delitem__(self, key):
#         self._polygon.__delitem__(key)
        
#     def __iter__(self):
#         return self._polygon.__iter__()




def is_support(el) -> bool:
    return isinstance(el, SVGPolygon) or \
        isinstance(el, SVGEllipse) or \
            isinstance(el, SVGPath) or \
            isinstance(el, Polygon)
            
def is_line(el) -> bool:
    points = get_points(el)
    x_arr = [p.x for p in points]
    y_arr = [p.y for p in points]
    left_idx = np.argmin(x_arr)
    right_idx = np.argmax(x_arr)
    up_idx = np.argmax(y_arr)
    down_idx = np.argmin(y_arr)
    x_vertical_distance = y_arr[left_idx] - y_arr[right_idx]
    y_horizontal_distance = x_arr[up_idx] - x_arr[down_idx]
    k = abs(x_vertical_distance / y_horizontal_distance)
    return 0.9 > k or k > 1.1
    # xlen = max(x_arr) - min(x_arr)
    # ylen = max(y_arr) - min(y_arr)
    # return max(xlen, ylen) / min(xlen, ylen) > 4

def get_points(el) -> List[Point]:
    match el:
        case SVGPolygon():
            return [Point(x, y) for x, y in el.points]
        case SVGEllipse():
            return [Point(x, y) for x, y in Ellipse((el.implicit_center.x, el.implicit_center.y), 
                                               el.rx, el.ry).get_verts()]
        case SVGPath():
            return [Point(x, y) for x, y in list(map(list, el.as_points()))]
        case Polygon():
            return [Point(x, y) for x, y in zip(*el.exterior.coords.xy)]


def find_zero_point(elements, str_prefix='0 ('):
    for el, attrs in elements:
        if 'startpoint' in  attrs.get('class', '').split(' '):
            p = el
            return p.centroid.x, p.centroid.y
    # classes = []
    # polygons_candidats = []
    # for el in elements:
    #     match el:
    #         case Text():
    #             if el.text.startswith(str_prefix):
    #                 classes.extend(el.values.get('class', '').split(' '))
    #                 for cl in classes:
    #                     if not (cl.startswith('fil') or cl.startswith('str')):
    #                         classes.remove(cl)
    #         case SVGPolygon():
    #             polygons_candidats.append(el)
    # classes = set(classes)
    # for pl in polygons_candidats:
    #     if set(pl.values.get('class').split(" ")).intersection(classes):
    #         p = Polygon([(x, y) for x, y in pl.points])
    #         elements.remove(pl)
    #         return p.exterior.coords[0]
    #         # return p.centroid.x, p.centroid.y
    # return find_zero_point(elements, str_prefix='0')


def distance_to_near_point(el: Polygon, tp: Point) -> float:
    return np.min([(tp.distance(p) if p is not None else float('inf')) for p in (get_points(el) or [float('inf')])])


def filter_objects(elements: List[Polygon]) -> List[Polygon]:
    # active_text_points = text_point.copy()
    # candidats_elements = elements.copy()
    # for t in active_text_points.copy():
    #     if (t.text.find('Вп') == -1  and 
    #         t.text.lower().find('ш') == -1 and
    #         t.text.lower().find('рас') == -1):
    #         active_text_points.remove(t)
    
    
    out_elements = []
    for el, attr in elements:
        classes = attr.get('class', '').split(' ')
        # if not is_support(el) or 'aicls1' not in classes or 'aicls2' not in classes:
        if not set(classes).intersection(FILTERED_CLASSES):
            continue
        out_elements.append((el, attr))
        # if not is_support(el) or is_line(el):
        #     continue
        # out_elements.append(el)
    
    # candidats_elements = out_elements
    # out_elements = []
    # for t in active_text_points:
    #     candidats = []
    #     for el in candidats_elements:
    #         if not is_support(el) or is_line(el):
    #             continue
    #         candidats.append((el, distance_to_near_point(el, Point(t.x, t.y))))
    #     candidats.sort(key=lambda x: x[1])
    #     out_elements.append(candidats[0][0])
    #     candidats_elements.remove(candidats[0][0])
    
    return out_elements


def get_scale_point(elements: List[Polygon]) -> tuple[Point, Point]:
    point1 = None
    point2 = None
    for el in elements:
        # if el.values.get('class', '').find('scalepoint1') != -1:
        if 'startpoint1' in  el.classes:
            point1 = Point(el.x1, el.y1)
            continue
        if 'startpoint2' in  el.classes:
        # if el.values.get('class', '').find('scalepoint2') != -1:
            point2 = Point(el.x1, el.y1)
            continue
        if point1 is not None and point2 is not None:
            break
    return point1, point2


def find_objects(elements) -> Dict[str, Polygon]:
    # group by class attr
    elements_by_class = defaultdict(list)
    for el, attr in elements:
        obj = None
        match el:
            case SVGPolygon():
                obj = Polygon([(x, y) for x, y in el.points])
                
            case SVGEllipse():
                obj = Polygon(Ellipse((el.implicit_center.x, el.implicit_center.y), 
                                               el.rx, el.ry).get_verts())     
            case SVGPath():
                # obj = LinearRing([el.point(i) for i in range(len(el))])   
                obj = Polygon([(x, y) for x, y in list(map(list, el.as_points()))])
            case Polygon():
                obj = el
                # obj = el
        if obj is not None:
            for c in set(attr.get('class', '').split(' ')).intersection(FILTERED_CLASSES):
                elements_by_class[c].append(obj)
    return elements_by_class


def normalize_objects(objects, zero_point) -> List[Polygon]:
    norm_objects = []
    for obj in objects:
        match obj:
            case LinearRing():
                norm_objects.append(LinearRing([(x - zero_point[0], y - zero_point[1]) for x, y in obj.coords]))
            case Polygon():
                norm_objects.append(Polygon([(x - zero_point[0], y - zero_point[1]) for x, y in obj.exterior.coords]))
            case SVGPath():
                norm_objects.append(MultiPoint([(x - zero_point[0], y - zero_point[1]) for x, y in list(map(list, obj.as_points()))]))
    return norm_objects


def coords_transform(obj: Polygon, pixels_in_one_meter: float) -> Polygon:
    return Polygon([(p.x / pixels_in_one_meter, p.y / pixels_in_one_meter) for p in get_points(obj)])

def transform_matrix_apply(obj: Polygon, matrix: List[List[float]]) -> Polygon:
    return Polygon([(x * matrix[0][0] + y * matrix[1][0], x * matrix[0][1] + y * matrix[1][1]) for x, y in obj.exterior.coords])


def get_norm_objects_by_class(elements: List[SVGPath]):
    # elements = list(svg.elements())
    # text_elements = list(filter(lambda el: isinstance(el, Text), elements))
    
    zero_point = find_zero_point(elements)
    # elements = list(filter(lambda el: not isinstance(el, SVGPolygon), elements))
    elements = filter_objects(elements)
    objects_by_class = find_objects(elements)
    norm_objects_by_class = {k: normalize_objects(v, zero_point) for k, v in objects_by_class.items() if k not in set(SYSTEMCLASS)}
    return {**objects_by_class, **norm_objects_by_class}

def main(file_content):
    svg = SVG.parse(file_content)
    # find point 0
    norm_objects_by_class = get_norm_objects_by_class(svg)
    # norm_objects_by_class = objects_by_class
    
    plt.figure()
    plt.plot(0, 0, 'ro', markersize=10)
    # plt.plot(zero_point[0], zero_point[1], 'ro', markersize=10)
    colors = mpl.colormaps['magma'].colors
    class_by_number = dict((k, num) for num, k in enumerate(set(norm_objects_by_class.keys())))
    for cls, obj_arr in norm_objects_by_class.items():
        for obj in obj_arr:
            match obj:
                # case LinearRing():
                #     plt.plot(*obj.coords.xy, color=colors[class_by_number[cls]])
                # case MultiPoint():
                #     plt.plot(*obj.envelope.exterior.xy, color=colors[class_by_number[cls]])
                case Polygon():
                    plt.plot(*obj.exterior.xy, color=colors[class_by_number[cls]])
    plt.show(block=True)


def get_path_points(el) -> List[Tuple[float, float]]:
    return list((el.point(1/i).real, el.point(1/i).imag) for i in range(1, min(1001, int(el.length()))))

def get_objects(directory) -> Dict[str, List[Polygon]]:
    out_objects = defaultdict(list)
    directory = Path(directory)
    svg_file = list(directory.glob('*.svg'))[0]
    # meta_file = directory / 'meta.json'
    elements, attr_elements = svg2paths(svg_file)
    elements = list(zip([
        Polygon(get_path_points(el)) 
        for el, attr in zip(elements, attr_elements)], attr_elements))

    norm_objects_by_class = get_norm_objects_by_class(elements)
    # pixels_in_one_meter = json.loads(meta_file.read_text())['pixelsInOneMeter']
    scale_points = norm_objects_by_class['scalepoint1'][0], norm_objects_by_class['scalepoint2'][0] #get_scale_point(elements)
    pixels_in_one_meter = scale_points[0].distance(scale_points[1]) / 10
    # transform_matrix = json.loads(meta_file.read_text()).get('transformMatrix')
    for cls, obj_arr in norm_objects_by_class.items():
        if cls not in set(AICLS):
            continue
        for obj in obj_arr:
            match obj:
                case Polygon():
                    # print('Polygon:', str(list(obj.exterior.coords)), 'to', coords_transform(obj, pixels_in_one_meter))
                    new_obj = coords_transform(obj, pixels_in_one_meter)
                    # if transform_matrix is not None:
                    #     new_obj = transform_matrix_apply(new_obj, transform_matrix)
                    out_objects[cls].append(new_obj)
    # print(out_objects)
    return out_objects



    
if __name__ == "__main__":
    from pathlib import Path
    # main((Path(__file__).parent.parent / 'примеры данных/Андреевское 1/андреевское 1 итоговая версия.svg'))
    get_objects(Path(__file__).parent.parent / 'примеры данных/Андреевское 1')