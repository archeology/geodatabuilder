import base64
from dataclasses import dataclass
import io
import json
from pathlib import Path
from typing import List, Tuple
import numpy as np
import streamlit as st
import plotly.express as px
import plotly.graph_objects as go
import pandas as pd
from parsers import magnitometr, tahiometr, svg
from enum import Enum
import matplotlib.pyplot as plt
from shapely import Polygon, Point
import scipy
import scipy.interpolate
from sklearn.cluster import AgglomerativeClustering
from sklearn.preprocessing import MinMaxScaler

CLS_LINETYPE = {
    'aicls1': '--r',
    'aicls2': '-.b',
    'aicls3': '-.g',
    'aicls4': ':r',
    'aicls5': '-.y',
    'aicls6': '-.m',
    'aicls7': '-.c',
}

@dataclass
class MetaInfo:
    pixels_in_one_meter: float
    transform_matrix: np.array
    inverse_x: bool
    inverse_y: bool
    
    def save(self, file_paath: str):
        obj: dict = json.loads(Path(file_paath).read_text())
        obj.update({
            'pixelsInOneMeter': self.pixels_in_one_meter,
            'transformMatrix': self.transform_matrix.tolist(),
            'inverseX': self.inverse_x,
            'inverseY': self.inverse_y})
        Path(file_paath).write_text(json.dumps(obj))
        
    @staticmethod
    def load(file_paath: str):
        file_paath = Path(file_paath)
        if file_paath.exists():
            obj = json.loads(Path(file_paath).read_text())
        else:
            obj = {}
        return MetaInfo(obj.get('pixelsInOneMeter', 1), obj.get('transformMatrix'), obj.get('inverseX', False), obj.get('inverseY', False))



def get_mesh_points(data: pd.DataFrame) -> Tuple[np.array, np.array]:
    # Assuming `data` is a DataFrame with columns 'x', 'y', and 'z'  
    xy = data[['x', 'y']].values
    xy = MinMaxScaler().fit_transform(xy)
    model = AgglomerativeClustering(n_clusters=None, distance_threshold=st.slider('Радиус', 0.0, 10.0, 0.1, 0.01, key='mesh-slider-radius'))
    points_count = st.slider('Количество точек', 1, 100, 10, 1)
    labels = model.fit_predict(xy)
    meshes = []
    grids = []
    xy = data[['x', 'y']].values
    for label in set(labels):
        mask = labels == label
        if sum(mask) < 4:
            meshes.append(xy[mask, :])
            grids.append(data['h'][mask].values)
            continue
        mesh = np.meshgrid(np.linspace(xy[mask, 0].min(), xy[mask, 0].max(), points_count), np.linspace(xy[mask, 1].min(), xy[mask, 1].max(), points_count))
        mesh = np.hstack((mesh[0].reshape(-1, 1), mesh[1].reshape(-1, 1)))
        grid = scipy.interpolate.griddata(xy[mask, :], data['h'][mask].values, mesh,)
        meshes.append(mesh)
        grids.append(grid)

    mesh = np.vstack(meshes)
    new_grid = np.hstack(grids)
    # mesh = np.meshgrid(np.linspace(data['x'].min(), data['x'].max(), 100), np.linspace(data['y'].min(), data['y'].max(), 100))
    # mesh = np.hstack((mesh[0].reshape(-1, 1), mesh[1].reshape(-1, 1)))
    # new_grid = scipy.interpolate.griddata(data[['x', 'y']].values, data['h'], mesh,)
    return mesh, new_grid

def get_surface_tahiometr(data: pd.DataFrame, object_of_interest, transform_matrix: np.array = None):
    # Assuming `data` is a DataFrame with columns 'x', 'y', and 'z'  
    fig = plt.figure()
    if st.checkbox('Интерполировать облако точек', value=False, key='plot-interpolate'):
        mesh, new_grid = get_mesh_points(data)
        plt.scatter(mesh[:, 0], mesh[:, 1], c=new_grid, s=1)
    else:
        plt.scatter(data['x'], data['y'], c=data['h'], s=3)
    # st.write(object_of_interest)
    is_double_poly = st.checkbox('Дублировать полигон', value=False, key='plot-poly')
    for cls, obj_arr in object_of_interest.items():
        # st.write(isinstance(obj, Polygon), type(obj))
        for obj in obj_arr:
                # st.write(obj)
                # if transform_matrix is not None:
                #     obj = svg.transform_matrix_apply(obj, transform_matrix)
                x = np.array(obj.exterior.coords.xy[0])
                y = np.array(obj.exterior.coords.xy[1])
                if is_double_poly: 
                    rot180 = np.array([[1, 0], [0, -1]])
                    # cx, cy = obj.exterior.centroid.xy
                    xy = [] 
                    for i in range(len(x)):
                        xy.append(np.dot(np.array([x[i], y[i]]), rot180))
                    xy = np.array(xy)
                    x = np.hstack((x, xy[:, 0]))
                    y = np.hstack((y, xy[:, 1]))
                plt.plot(x, y, CLS_LINETYPE[cls], linewidth=2)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.colorbar()

    # Display the plot in Streamlit
    st.pyplot(fig)


def get_surface_magnitometr(data: pd.DataFrame):
    # Assuming `data` is a DataFrame with columns 'x', 'y', and 'z'  
    z = st.selectbox('Выбор частоты', data['z'].unique())
    data = data[data['z'] == z]
    fig = plt.figure() 
    plt.scatter(data['x'], data['y'], c=data['value'])
    plt.xlabel('x')
    plt.ylabel('y')
    plt.colorbar()
    st.pyplot(fig)

class InstrumentType(Enum):
    TAHIOMETER = "Тахеометр"
    MAGNITOMETER = "Магнитометр"


device_type: InstrumentType = None

content_txt = None
object_of_interest = None
dir_select = st.selectbox('Select directory', list(map(lambda p: str(p.parent), Path('dataset').rglob('*.svg'))))
if dir_select:
    file = list(filter(lambda p: p.stem.find('чист') == -1, Path(dir_select).rglob('*.txt')))[0]
    content_txt = file.read_text()
    object_of_interest = svg.get_objects(Path(dir_select))
if content_txt:
    device_type = None
    try:
        data = magnitometr.parse(content_txt)
        device_type = InstrumentType.MAGNITOMETER
    except:
        data = tahiometr.parse(content_txt)
        device_type = InstrumentType.TAHIOMETER

if device_type:
    st.write(f'Тип прибора: {device_type.value}')
    st.write(data)

    if device_type == InstrumentType.MAGNITOMETER:
        st.write(get_surface_magnitometr(data))
    else:
        inverse_y = st.checkbox('Инверсия оси y', value=MetaInfo.load(Path(dir_select) / 'meta.json').inverse_y)
        if inverse_y:
            for cls, obj_arr in object_of_interest.items():
                for i, _ in enumerate(obj_arr):
                    object_of_interest[cls][i] = Polygon([(x, -y) for x, y in obj_arr[i].exterior.coords])
        inverse_x = st.checkbox('Инверсия оси x', value=MetaInfo.load(Path(dir_select) / 'meta.json').inverse_x)
        if inverse_x:
            for cls, obj_arr in object_of_interest.items():
                for i, _ in enumerate(obj_arr):
                    object_of_interest[cls][i] = Polygon([(-x, y) for x, y in obj_arr[i].exterior.coords])
        angle = st.slider('Выбор угла поворота', -180, 180, 0, step=5)
        transform_matrix = np.array([[np.cos(np.deg2rad(angle)), -np.sin(np.deg2rad(angle))], [np.sin(np.deg2rad(angle)), np.cos(np.deg2rad(angle))]])
        get_surface_tahiometr(data, object_of_interest, transform_matrix)
        
        if st.button('Сохранить', type='primary', use_container_width=True):
            mesh, h = get_mesh_points(data)
            mask = np.zeros_like(h, dtype=bool)
            save_data = pd.DataFrame({'x': mesh[:, 0], 'y': mesh[:, 1], 'h': h, 'isObject': mask, 'objectType': '0'})
            for i in range(save_data.shape[0]):
                for cls, obj_arr in object_of_interest.items():
                    for obj in obj_arr:
                        if obj.contains(Point(mesh[i, 0], mesh[i, 1])):
                            save_data['isObject'][i] = True
                            save_data['objectType'][i] = str(svg.AICLS.index(cls))
            save_data.to_csv(Path(dir_select) / f'{Path(dir_select).stem}.csv')